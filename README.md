# The Unusual Academic Library
This library focuses on mental health, accesibility and neurodiversity, often with connection to academia.
- [Zoë J. Ayres, Managing your Mental Health during your PhD: A Survival Guide](https://www.amazon.com/Managing-your-Mental-Health-during/dp/3031141938/)
- [Bernadette "bird" Bowen, I have been the bad guy: Neuroqueer Self-Realizations in the Algorithmic Age](https://www.amazon.com/dp/B0CD16CHDX)
- [Nicole Brown & Jennifer Leigh, Ableism in Academia: Theorising experiences of disabilities and chronic illnesses in higher education](https://www.jstor.org/stable/j.ctv13xprjr)
- [Paul Ellis, Amanda Kirby & Abby Osborne, Neurodiversity and Education](https://www.amazon.com/Neurodiversity-Education-Paul-Ellis/dp/1529600359/)
- [Victoria Honeybourne, The Neurodiverse Workplace: An Employer's Guide to Managing and Working with Neurodivergent Employees, Clients and Customers](https://www.amazon.com/Neurodiverse-Workplace-Employers-Neurodivergent-Employees/dp/1787750337/)
- [Kay R. Jamison, An Unquiet Mind: A Memoir of Moods and Madness](https://www.amazon.com/Unquiet-Mind-Memoir-Moods-Madness/dp/0679763309)
- [Kay R. Jamison, Nothing Was the Same: A Memoir](https://www.amazon.com/Nothing-Was-Same-Redfield-Jamison/dp/0307277895)
- [Amanda Kirby & Theo Smith, Neurodiversity at Work: Drive Innovation, Performance and Productivity with a Neurodiverse Workforce](https://www.amazon.com/Neurodiversity-Work-Performance-Productivity-Neurodiverse/dp/1398600245/)
- [Oliver Sacks, An Anthropologist on Mars](https://www.amazon.com/Anthropologist-Mars-English-Oliver-Sacks-ebook/dp/B00569FOT2)
- [Elyn R. Saks, The Center Cannot Hold: My Journey Through Madness](https://www.amazon.com/Center-Cannot-Hold-Journey-Through-ebook/dp/B00JJ9QJ94/)
- [Steve Silberman, Neurotribes: The Legacy of Autism and the Future of Neurodiversity](https://www.amazon.com/NeuroTribes-Steve-Silberman-audiobook/dp/B013RK2D90/)

